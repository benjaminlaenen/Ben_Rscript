multiMedusaSummary1 <- edit(multiMedusaSummary)

function (res, conTree, cutOff = 0.05, plotModelSizes = TRUE, 
    plotTree = TRUE, cex = 0.5, resolveConTree = FALSE, ...) 
{
    richness <- res$richness
    results <- res$results
    medusaVersion <- res$medusaVersion
    if (is.null(medusaVersion)) 
        medusaVersion <- "< 0.93.4.19"
    richness <- formatRichness(richness)
    conTree <- prepareData(phy = conTree, richness = richness, 
        verbose = FALSE, resolveTree = resolveConTree)$phy
    conTree <- manageTipLabels(c(results[[1]]$phy, conTree))[[2]]
    conTree <- ladderize(conTree)
    num.trees <- length(results)
    cat("Summarizing MEDUSA results across ", num.trees, " trees.\n\n", 
        sep = "")
    if (length(unique(lapply(lapply(results, FUN = "[[", "phy"), 
        FUN = "[[", "tip.label"))) == 1 && identical(results[[1]]$phy$tip.label, 
        conTree$tip.label)) {
        cat("All translation tables identical. Summary straightforward.\n\n")
    }
    else {
        stop("Not all translation tables identical. Functionality not yet implemented.\n\n")
    }
    n.tips <- length(conTree$tip.label)
    num.edges <- length(conTree$edge[, 1])
    root.node <- n.tips + 1
    obj <- makeCacheMedusa(phy = conTree, richness = richness, 
        all.nodes = seq_len((2 * n.tips) - 1), shiftCut = "both", 
        verbose = FALSE, mc = F)
    con.desc <- list(stem = obj$desc.stem, node = obj$desc.node)
    con.z <- obj$z
    con.edge.tip.desc <- lapply(con.z[, "dec"], FUN = getTips, 
        z = con.z, desc = con.desc$stem, n.tips = n.tips)
    model.sizes <- numeric(num.trees)
    for (i in 1:length(results)) {
        model.sizes[i] <- length(results[[i]]$optModel$split.at)
    }
    mean.n.models <- mean(model.sizes)
    sd.n.models <- sd(model.sizes)
    min.n.models <- min(model.sizes)
    max.n.models <- max(model.sizes)
    if (plotModelSizes) {
        hist(model.sizes, main = NULL, xlab = "Number of Piecewise Models", 
            prob = TRUE)
        lines(density(model.sizes, adjust = 2), lty = "longdash", 
            col = "red")
    }
    est.pars <- matrix(ncol = (2 * num.trees), nrow = num.edges)
    colnames(est.pars) <- rep(c("r", "epsilon"), num.trees)
    n.shifts <- sum(model.sizes) - num.trees
    est.splits <- rep(NA, n.shifts)
    est.cuts <- rep(NA, n.shifts)
    est.shift.magnitudes.r <- rep(NA, n.shifts)
    est.shift.magnitudes.b <- rep(NA, n.shifts)
    est.shift.magnitudes.d <- rep(NA, n.shifts)
    est.shift.magnitudes.eps <- rep(NA, n.shifts)
    indx.pos <- 1
    for (i in 1:num.trees) {
        i.z <- results[[i]]$optModel$z
        i.edge.tip.desc <- lapply(i.z[, "dec"], FUN = getTips, 
            z = i.z, desc = results[[i]]$desc$stem, n.tips = n.tips)
        i.par <- results[[i]]$optModel$par
        i.splits <- results[[i]]$optModel$split.at[-1]
        i.cuts <- results[[i]]$optModel$cut.at[-1]
        idx.conToRep <- match(con.edge.tip.desc, i.edge.tip.desc)
        idx.repToCon <- match(i.edge.tip.desc, con.edge.tip.desc)
        est.pars[, c(((2 * i) - 1), (2 * i))] <- i.par[i.z[idx.conToRep, 
            "partition"], ]
        if (length(i.splits) > 0) {
            mapped.splits <- rep(NA, length(i.splits))
            for (d in 1:length(i.splits)) {
                if (i.cuts[d] == "node") {
                  mapped.splits[d] <- as.integer(con.z[idx.repToCon[which(i.z[, 
                    "dec"] == i.splits[d])], "dec"])
                }
                else {
                  mapped.splits[d] <- as.integer(con.z[idx.repToCon[which(i.z[, 
                    "dec"] == i.splits[d])], "anc"])
                }
            }
            shift.magnitudes.r <- rep(NA, length(i.splits))
            shift.magnitudes.b <- rep(NA, length(i.splits))
            shift.magnitudes.d <- rep(NA, length(i.splits))
            shift.magnitudes.eps <- rep(NA, length(i.splits))
            mappable.magnitude <- TRUE
            for (k in 1:length(i.splits)) {
                if (!is.na(mapped.splits[k])) {
                  parent.class <- NULL
                  descendant.class <- NULL
                  mappable.magnitude <- TRUE
                  if (i.cuts[k] == "stem" && mapped.splits[k] != 
                    root.node) {
                    parent.node <- as.integer(i.z[which(i.z[, 
                      "dec"] == i.splits[k]), "anc"])
                    if (parent.node != root.node) {
                      parent.class <- as.integer(i.z[which(i.z[, 
                        "dec"] == parent.node), "partition"])
                      descendant.class <- as.integer(i.z[which(i.z[, 
                        "dec"] == i.splits[k]), "partition"])
                    }
                    else {
                      mappable.magnitude <- FALSE
                    }
                  }
                  else {
                    parent.class <- as.integer(i.z[which(i.z[, 
                      "dec"] == i.splits[k]), "partition"])
                    descendant.class <- k + 1
                  }
                  if (mappable.magnitude) {
                    parms <- i.par[c(parent.class, descendant.class), 
                      ]
                    shift.magnitudes.r[k] <- as.numeric(parms[2, 
                      "r"] - parms[1, "r"])
                    if (any(!is.na(parms[, "epsilon"]))) {
                      parms[which(is.na(parms[, 2])), 2] <- 0
                      shift.magnitudes.eps[k] <- as.numeric(parms[2, 
                        "epsilon"] - parms[1, "epsilon"])
                      parent.bd <- getBD(as.numeric(i.par[parent.class, 
                        1]), as.numeric(i.par[parent.class, 2]))
                      descendant.db <- getBD(as.numeric(i.par[descendant.class, 
                        1]), as.numeric(i.par[descendant.class, 
                        2]))
                      shift.magnitudes.b[k] <- as.numeric(descendant.db$b - 
                        parent.bd$b)
                      shift.magnitudes.d[k] <- as.numeric(descendant.db$d - 
                        parent.bd$d)
                    }
                  }
                }
            }
            for (j in 1:length(i.splits)) {
                est.shift.magnitudes.r[indx.pos] <- shift.magnitudes.r[j]
                est.shift.magnitudes.eps[indx.pos] <- shift.magnitudes.eps[j]
                est.shift.magnitudes.b[indx.pos] <- shift.magnitudes.b[j]
                est.shift.magnitudes.d[indx.pos] <- shift.magnitudes.d[j]
                est.splits[indx.pos] <- mapped.splits[j]
                est.cuts[indx.pos] <- i.cuts[j]
                indx.pos <- indx.pos + 1
            }
        }
    }
    rates <- matrix(ncol = 9, nrow = num.edges)
    colnames(rates) <- c("r.mean", "r.median", "r.sd", "eps.mean", 
        "eps.median", "eps.sd", "freq","5","95")
    for (i in 1:num.edges) {
        i.r <- as.numeric(est.pars[i, seq(from = 1, to = (num.trees * 
            2), by = 2)])
        idx.valid <- !is.na(i.r)
        i.eps <- as.numeric(est.pars[i, seq(from = 2, to = (num.trees * 
            2), by = 2)])
        i.eps <- i.eps[idx.valid]
        i.eps[which(is.na(i.eps))] <- 0
        r.mean <- mean(i.r, na.rm = TRUE)
        r.median <- median(i.r, na.rm = TRUE)
        r.sd <- sd(i.r, na.rm = TRUE)
        eps.mean <- mean(i.eps)
        eps.median <- median(i.eps)
        eps.sd <- sd(i.eps)
        freq <- sum(idx.valid)/num.trees
       CI5 <- quantile(i.r, probs=0.05, na.rm = TRUE)
       CI95 <- quantile(i.r, probs=0.95, na.rm = TRUE)
       
        rates[i, ] <- c(r.mean, r.median, r.sd, eps.mean, eps.median, 
            eps.sd, freq,CI5,CI95)
            
                }
    mapping <- match(conTree$edge[, 2], con.z[, 2])
    conTree$rates <- rates[mapping, ]
    idx.valid <- which(!is.na(est.splits))
    shift.pos <- as.data.frame(cbind(est.splits[idx.valid], est.cuts[idx.valid]))
    shift.summary <- data.frame(cbind(shift.node = as.integer(rownames(table(shift.pos))), 
        table(shift.pos)/num.trees))
    if (length(shift.summary[1, ]) < 3) {
        if (is.null(shift.summary$node)) {
            shift.summary <- cbind(shift.node = shift.summary[, 
                1], node = rep(0, length(shift.summary[, 1])), 
                stem = shift.summary$stem)
        }
        else if (is.null(shift.summary$stem)) {
            shift.summary <- cbind(shift.summary[, 1:2], stem = rep(0, 
                length(shift.summary[, 1])))
        }
        else {
            stop("\nUm, I don't know what is wrong here.\n")
        }
    }
    colnames(shift.summary)[2:3] <- c("cut.at.node", "cut.at.stem")
    unique.shifts <- shift.summary[, "shift.node"]
    num.unique.shifts <- length(unique.shifts)
    mean.shift <- rep(NA, num.unique.shifts)
    median.shift <- rep(NA, num.unique.shifts)
    min.shift <- rep(NA, num.unique.shifts)
    max.shift <- rep(NA, num.unique.shifts)
    sd.shift <- rep(NA, num.unique.shifts)
   CI5.shift <- rep(NA, num.unique.shifts)
   CI95.shift <- rep(NA, num.unique.shifts)
    for (i in 1:length(unique.shifts)) {
        idx.shift <- which(est.splits == unique.shifts[i])
        cur.shift.mag <- est.shift.magnitudes.r[idx.shift]
        mean.shift[i] <- mean(cur.shift.mag, na.rm = TRUE)
        median.shift[i] <- median(cur.shift.mag, na.rm = TRUE)
        min.shift[i] <- min(cur.shift.mag, na.rm = TRUE)
        max.shift[i] <- max(cur.shift.mag, na.rm = TRUE)
        sd.shift[i] <- sd(cur.shift.mag, na.rm = TRUE)
        CI5.shift[i] <- quantile(cur.shift.mag, probs=0.05, na.rm = TRUE)
        CI95.shift[i] <- quantile(cur.shift.mag, probs=0.95, na.rm = TRUE)

    }
     bt <- branching.times(conTree)
    time = bt[as.character(shift.summary[, 1])]
    shift.summary <- cbind(shift.node = shift.summary[, 1], sum.prop = (shift.summary[, 
        "cut.at.node"] + shift.summary[, "cut.at.stem"]), mean.shift = mean.shift, 
        median.shift = median.shift, min.shift = min.shift, max.shift = max.shift, 
        sd.shift = sd.shift, CI5=CI5.shift,CI95=CI95.shift, Time=time)
    shift.summary <- shift.summary[order(shift.summary[, "sum.prop"], 
        decreasing = TRUE), ]
    shift.summary <- shift.summary[which(shift.summary[, "sum.prop"] >= 
        cutOff), , drop = FALSE]
    rownames(shift.summary) <- NULL
    shifts <- as.data.frame(cbind(node = est.splits, shift.r = est.shift.magnitudes.r, 
        shift.eps = est.shift.magnitudes.eps, shift.b = est.shift.magnitudes.b, 
        shift.d = est.shift.magnitudes.d))
    shifts <- shifts[-which(is.na(shifts$node)), ]
    idx <- shift.summary[, 1]
    foo <- function(id) {
        tmp <- shifts[which(shifts$node == id), ]
        r <- tmp$shift.r
        eps <- tmp$shift.eps
        b <- tmp$shift.b
        d <- tmp$shift.d
        node <- id
        return(list(node = node, r = r, eps = eps, b = b, d = d))
    }
    shifts <- lapply(idx, foo)
    if (length(shift.summary) == 0) {
        cat("WARNING: no node shifts occur above cutoff of ", 
            cutOff, ". Try setting lower cutoff.\n", sep = "")
    }
    if (cutOff > 0) {
        cat("Mapped rate shift positions present in at least ", 
            cutOff * 100, "% (of ", num.trees, " total) trees:\n\n", 
            sep = "")
    }
    else {
        cat("Mapped rate shift positions across all ", num.trees, 
            " trees:\n\n", sep = "")
    }
    print(shift.summary)
    summary <- list(model.sizes = model.sizes, num.trees = num.trees, 
        shift.summary = shift.summary, summary.tree = conTree, 
        richness = richness, shift.magnitudes = shifts, medusaVersion = medusaVersion)
    class(summary) <- "multiMedusaSummary"
    if (plotTree) 
        plotMultiMedusa(summary, ...)
    invisible(summary)
}
