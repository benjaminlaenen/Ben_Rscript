library(scatterplot3d)

div <- read.table(file.choose(), header=T)


#diversification rate versus time
#crown group eps0
plot(-div[21:24,2],div[21:24,4],col="green", ylim= c(0, 0.15), xlim=c(-500, 0), pch=4)
points(-div[1:8,2],div[1:8,4],ylim= c(0, 0.2), pch=8)
points(-div[9:19,2],div[9:19,4], col="red", pch=21)
points(-div[20,2],div[20,4],col="blue", pch=22)
points(-div[25:27,2],div[25:27,4],col="cyan", pch=4)


#crown group eps0.9
plot(-div[21:24,2],div[21:24,5],col="green", ylim= c(0, 0.15), xlim=c(-500, 0), pch=4)
points(-div[1:8,2],div[1:8,5],ylim= c(0, 0.2), pch=8)
points(-div[9:19,2],div[9:19,5], col="red", pch=21)
points(-div[20,2],div[20,5],col="blue", pch=22)
points(-div[25:27,2],div[25:27,5],col="cyan", pch=4)

#stem group eps0
plot(-div[21:24,3],div[21:24,6],col="green", ylim= c(0, 0.15), xlim=c(-500, 0), pch=4)
points(-div[1:8,3],div[1:8,6],ylim= c(0, 0.2), pch=8)
points(-div[9:19,3],div[9:19,6], col="red", pch=21)
points(-div[20,3],div[20,6],col="blue", pch=22)
points(-div[25:27,3],div[25:27,6],col="cyan", pch=4)

#stem group eps0.9
plot(-div[21:24,3],div[21:24,7],col="green", ylim= c(0, 0.2), xlim=c(-500, 0), pch=4)
points(-div[1:8,3],div[1:8,7],ylim= c(0, 0.2), pch=8)
points(-div[9:19,3],div[9:19,7], col="red", pch=21)
points(-div[20,3],div[20,7],col="blue", pch=22)
points(-div[25:27,3],div[25:27,7],col="cyan", pch=4)

