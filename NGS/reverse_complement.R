rev.comp<-function(x,reverse=TRUE){
    x<-toupper(x)

    X <- unlist(strsplit(x, ""))

    X[X == "A"] <- "t"
    X[X == "C"] <- "g"
    X[X == "G"] <- "c"
    X[X == "T"] <- "a"

    if(reverse) X <- rev(X)
    X <- toupper(X)

    return(paste(X, collapse = ""))
}


readClip <- function() readLines(pipe("pbpaste"), warn =FALSE)
readClip()
