#!/usr/bin/env Rscript
#doing haplotypes datafile

args <- commandArgs(TRUE)

suppressMessages((library(ape)))
suppressMessages((library(pegas)))


y <- read.dna(args[[1]], format='fasta')
y

h <- haplotype(y)
h

cat("Writing haplotypes fasta file to hap.fas", file=stderr())
write.dna(h, file="hap.fas", format="fasta")

seq_names <- attr(y, "dimnames")[[1]]


hap <- list()
for(i in 1:length(row.names(h)))
{
hap[[i]] <- attr(h, "index")[[i]]
}

hap_seq_names <- list()
for(i in 1:length(row.names(h)))
{
hap_seq_names[[i]] <- seq_names[hap[[i]]]
}

hap_list <- attr(h, "dimnames")[[1]]

names(hap_seq_names) <-  hap_list 
names(hap) <-  hap_list

for(i in 1:length(hap)){
	cat(hap_list[i], " : ", file=stdout())
	cat(paste(hap[[i]]),  "\n", file=stdout())
}

for(i in 1:length(hap)){
	cat(hap_list[i], " : ", file=stdout())
	cat(paste(hap_seq_names[[i]]), "\n", file=stdout())
}