library(phyloch)
#read.beast
library(epibase)
#read.annotated.nexus
liv_beast <- read.beast(file.choose())
terms <- liv_beast$edge[, 2] <= Ntip(liv_beast)
terminal.edges <- liv_beast$edge.length[terms]
names(terminal.edges) <- liv_beast$tip.label[liv_beast$edge[terms, 2]]
###problem edge.length has twice elements (edge) than height HPD (nodes)
#terminal.edges_max <- liv_beast$"height_95%_HPD_MAX"[terms]
#names(terminal.edges_max) <- liv_beast$tip.label[liv_beast$edge[terms, 2]]
#terminal.edges_min <- liv_beast$"height_95%_HPD_MIN"[terms]
#names(terminal.edges_min) <- liv_beast$tip.label[liv_beast$edge[terms, 2]]


tre <- read.annotated.nexus(file.choose())
#Annotations are ordered by edges, i.e. matching the edge.length slot of a phylo object. ---> on peut extraire via terms
terms <- tre$edge[, 2] <= Ntip(tre)
tre_terminal.edges <-  tre$edge.length[terms]
names(tre_terminal.edges) <- liv_beast$tip.label[liv_beast$edge[terms, 2]]

#test if identical between the two loading tree
identical(tre_terminal.edges, terminal.edges)

#use of the median terminal edge length, hence can be different from median node height.
#annotation are stored as list of list following the structure of edge.length
median_length_age <- unlist(sapply(tre$annotations, function(e) e$length_median))
terminal.edges_median <- median_length_age[terms]
#problem with naming (must resolved) --> solution (not pretty) using names from the tree from read.beast
names(terminal.edges_median) <- liv_beast$tip.label[liv_beast$edge[terms, 2]]

#extracting max 95 hpd for terminal edge length
hpd95_Max <- unlist(sapply(tre$annotations, function(e) e$`length_95%_HPD`[[2]]))
terminal.edges_max <-  hpd95_Max[terms]
names(terminal.edges_max) <- liv_beast$tip.label[liv_beast$edge[terms, 2]]

hpd95_Min <- unlist(sapply(tre$annotations, function(e) e$`length_95%_HPD`[[1]]))
terminal.edges_min <-  hpd95_Min[terms]
names(terminal.edges_min) <- liv_beast$tip.label[liv_beast$edge[terms, 2]]

results_genus_age <- round(data.frame(edge.length_beast=terminal.edges, edge.length= tre_terminal.edges, median_length= terminal.edges_median, max95HPD= terminal.edges_max,min95HPD= terminal.edges_min ),2)