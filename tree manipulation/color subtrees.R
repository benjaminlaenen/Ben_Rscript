library(phytools)
> tree<-paintSubTree(tree,node=22,state="2")
> tree<-paintSubTree(tree,node=26,state="3")
> # now let's plot using plotSimmap to ensure
> # that the correct branches were painted
> cols<-c("black","blue","red"); names(cols)<-1:3
> plotSimmap(tree,cols,pts=F,lwd=3,node.numbers=T)
