Hi Simon.

By this, do you mean the age of the start of the edge leading to the 
MRCA of a set of taxa? This is easy using phytools.

If you want to get this age as the height above the root:

H<-nodeHeights(tree)
## tips are the species in the clade, or a subset of the species
## such that the MRCA of tips is the MRCA of the clade
nn<-findMRCA(tree,tips)
h<-H[tree$edge==phytools:::getAncestors(tree,nn,"parent")][1]

Use of getAncestors requires a recent non-CRAN version of phytools - 
otherwise you can load phangorn & substitute Ancestors.

If you want the time from the present in an ultrametric tree, just do:

H<-max(H)-H

and then repeat the same.

library(phangorn)




diviser un nom en deux collones
G <- gymno_tree$tip.label
gymno_df_genus <- read.table(text=gymno_tree$tip.label, sep="_")
gymno_unique_genus <- unique(as.character(gymno_df_genus[,1]))
#creer un liste unique de noms

H<-nodeHeights(gymno_tree)
#date from present
H<-max(H)-H

tips <- G[grep(gymno_unique_genus[1],G)]
nn<-findMRCA(gymno_tree,tips)
h<-H[gymno_tree$edge==Ancestors(gymno_tree,nn,"parent")][1]
